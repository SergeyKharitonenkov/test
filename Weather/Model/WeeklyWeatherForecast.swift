//
//  WeeklyWeatherForecast.swift
//  Weather
//
//  Created by MacBook on 09.07.2021.
//

import Foundation
import Alamofire
import SwiftyJSON


class WeeklyWaetherForecast{
    private var _date : Date!
    private var _temp : Double!
    private var _weatherIcon : String!
    
    var date: Date{
        if _date == nil{
            _date = Date()
        }
        return _date
    }
    
    var temp: Double{
        if _temp == nil{
            _temp = 0.0
        }
        return _temp
    }
    
    var weatherIcon: String{
        if _weatherIcon == nil{
            _weatherIcon = ""
        }
        return _weatherIcon
    }
    
    
    init(weatherDictionary: Dictionary<String, AnyObject>){
        
        let json = JSON(weatherDictionary)
        
        self._temp = json["temp"].double
        self._date = currentDateFromUnix(unixDate: json["ts"].double!)
        self._weatherIcon = json["weather"]["icon"].stringValue
    }
    class func downloadWeeklyWeatherForecast(completion: @escaping (_ weatherForecast : [WeeklyWaetherForecast])->Void){
        
        let WEEKLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/daily?city=Zaporozhye,UA&days=7&key=b4232e9240dd47a6ae7b2874f760528b"
        
        Alamofire.request(WEEKLYFORECAST_URL).responseJSON { (response) in
            let result = response.result
            var forecastArray: [WeeklyWaetherForecast] = []
            if result.isSuccess{
                 
                print("Weekly Date", result.value)
               
                
            }else{
                
                completion(forecastArray)
                print("no weekly forecast")
                
            }
        }
    }
}
