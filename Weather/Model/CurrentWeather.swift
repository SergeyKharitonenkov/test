

import Foundation
import Alamofire
import SwiftyJSON


class CurrentWeather {
    private var _city : String!
    private var _date : Date!
    private var _currentTempo : Int!
    
    
    
    private var _humidity: Int! //%
    private var _windSpeed:Int! //meter/sec
    private var _weatherIcon:String!
    
    
    
    var city: String{
        if _city == nil{
            _city = ""
        }
        return _city
    }
    
    var date:Date{
        if _date == nil{
            _date = Date()
        }
        return _date
    }
    var currentTempo: Int{
        if _currentTempo == nil{
            _currentTempo = 0
        }
        return _currentTempo
    }
   
   
    var humidity : Int{
        if _humidity == nil{
            _humidity = 0
        }
        return _humidity
    }
    var windSpeer : Int{
        if _windSpeed == nil{
            _windSpeed = 0
        }
        return _windSpeed
    }
    var weatherIcon: String{
        if _weatherIcon == nil{
            _weatherIcon = ""
        }
        return _weatherIcon
    }
   
    
    
        
        
    func getCurrentWeather(completion: @escaping(_ success: Bool)->Void) {
         let LOCATIONAPI_URL = "https://api.weatherbit.io/v2.0/current?city=Zaporozhye&key=b4232e9240dd47a6ae7b2874f760528b"

            Alamofire.request(LOCATIONAPI_URL).responseJSON { (response) in
                let result = response.result
                if result.isSuccess{
                 
                    let json = JSON(result.value)
                   
                    
                    
                    self._city = json["data"][0]["city_name"].stringValue
                    self._date = currentDateFromUnix(unixDate: json["data"][0]["ts"].double)
                    self._currentTempo = json["data"][0]["temp"].int
                    self._humidity = json["data"][0]["rh"].int
                    self._windSpeed = json["data"][0]["wind_spd"].int
                    self._weatherIcon = json["data"][0]["icon"].stringValue
                    
                    
                    completion(true)
                    
                }else{
                    completion(false)
                    print("Для текущего метоположения результатов не найдено")
                }
            }
        }
        
    }

