//
//  ForecastCollectionViewCell.swift
//  Weather
//
//  Created by MacBook on 13.07.2021.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    
    
    @IBOutlet weak var tempLabel: UILabel!
    
    
    @IBOutlet weak var weatherIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func generateCell(forecast: HourlyForecast) {
        
        timeLabel.text = forecast.date.time()
        weatherIconImageView.image = getWeatherIconFor(forecast.weatherIcon)
        tempLabel.text = "\(forecast.temp)°C"
        
        
    }
    
    
}
