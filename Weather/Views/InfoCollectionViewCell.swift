//
//  InfoCollectionViewCell.swift
//  Weather
//
//  Created by MacBook on 15.07.2021.
//

import UIKit

class InfoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var infoImageView: UIImageView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func generateCell(weatherInfo: WeatherInfo){
        infoLabel.text = weatherInfo.infoText
        infoLabel.adjustsFontSizeToFitWidth = true
        infoImageView.image = weatherInfo.image
        
    }
    
}
