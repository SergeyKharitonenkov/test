//
//  WeatherTableViewCell.swift
//  Weather
//
//  Created by MacBook on 15.07.2021.
//

import UIKit

class WeatherTableViewCell: UITableViewCell{
    @IBOutlet weak var dayOfTheWeekLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    

    @IBOutlet weak var weatherIconImageView: UIImageView!
    
    override func awakeFromNib() {super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
        
        func generateCell(weather: WeeklyWaetherForecast) {
            
            dayOfTheWeekLabel.text = weather.date.dayOfTheWeek()
            weatherIconImageView.image = getWeatherIconFor(weather.weatherIcon)
            tempLabel.text = "\(weather.temp)"
            
        
    }
    
}
