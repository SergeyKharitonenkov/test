//
//  WeatherViewController.swift
//  Weather
//
//  Created by MacBook on 12.07.2021.
//

import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var weatherScrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
}
    override func viewDidAppear(_ animated: Bool) {
      
        let weatherView = WeatherView()
        weatherView.frame = CGRect(x: 0, y: 0, width: weatherScrollView.bounds.width, height: weatherScrollView.bounds.height)
        weatherScrollView.addSubview(weatherView)
        
        weatherView.currentWeather = CurrentWeather()
        weatherView.currentWeather.getCurrentWeather { (success) in
            
            
            weatherView.refreshData()
        }
        
        getCurrentWeather(weatherView: weatherView)
        getWeeklyWeather(weatherView:weatherView)
        getHourlyWeather(weatherView: weatherView)
        
        }

    }
    
private func getCurrentWeather(weatherView: WeatherView){
    
    weatherView.currentWeather = CurrentWeather()
    weatherView.currentWeather.getCurrentWeather { (success) in
        weatherView.refreshData()
    }
        
    }
    
    private func getWeeklyWeather(weatherView: WeatherView){
        
        WeeklyWaetherForecast.downloadWeeklyWeatherForecast { (weatherForecasts) in
            weatherView.weeklyWeatherForecastData = weatherForecasts
            weatherView.tableView.reloadData()

        }
        
    }

    private func getHourlyWeather(weatherView: WeatherView){
        
        HourlyForecast.downloadHourlyForecastWeather { (weatherForecasts) in
            weatherView.daylyWeatherForecastData = weatherForecasts
            weatherView.hourlyCollectionView.reloadData()

        }
        
    }
    
    
    

