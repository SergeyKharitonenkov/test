//
//  File.swift
//  Weather
//
//  Created by MacBook on 15.07.2021.
//

import UIKit
import Foundation

struct WeatherInfo {
    var infoText: String!
    var nameText: String?
    var image: UIImage?
}
